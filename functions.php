
<?php
 
function bootstrap_enqueue_styles() {
    wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() . '/css/bootstrap-3.3.7.css' );
	wp_enqueue_style( 'main_css', get_template_directory_uri() . '/style.css' );

	wp_register_style( 'foundation_icons', 'STYLESHEETS URL' );
}
 
function bootstrap_enqueue_scripts() {
    global $wp_scripts;

	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/js/bootstrap.min.js' );
}
 
add_action( 'wp_enqueue_scripts', 'bootstrap_enqueue_styles' );
add_action( 'wp_enqueue_scripts', 'bootstrap_enqueue_scripts' );
 

function custom_scripts() {
	wp_enqueue_script( 'matchHeight_js', get_template_directory_uri() . '/vendor/js/jquery.matchHeight.js', 'all');
}
add_action( 'wp_enqueue_scripts', 'custom_scripts' );

function custom_styles() {
	wp_enqueue_style( 'style', get_stylesheet_directory_uri().'/vendor/css/foundation-icons.css', 'all');
}
add_action( 'wp_enqueue_scripts', 'custom_styles' );


function bootstrap_wp_setup() {
    add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'bootstrap_wp_setup' );

add_theme_support('woocommerce');

/**
 * Change number or products per row to 3
 */
add_filter('loop_shop_columns', 'loop_columns');

if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}

/**
 * Remove product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab

    return $tabs;
}
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );

/**
 * Hide product price
 */
/*add_filter( 'woocommerce_get_price_html', function( $price ) {
	if ( is_admin() ) return $price;

	return '';
} );*/


/**
 * Show cart contents / total Ajax
 */
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();

	?>
	<a class="nav-cart" href="<?php echo esc_url(wc_get_cart_url()); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>">
		<?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?>
	</a>
	<?php
	$fragments['a.cart-customlocation'] = ob_get_clean();
	return $fragments;
}

/*-- 08/08/2018 --*/
/**
 * Show header image across all pages
 */
function get_header_image_custom() {
	static $media_url;

    // Checks if function has already run
    if ( $media_url == null ) {
	    $frontpage_id=get_option( 'page_on_front' );
		$header_image='header_image';
		global $wpdb;
		// Retrieves front page post id
		$query= "SELECT meta_value FROM wp_postmeta WHERE post_id = '".$frontpage_id."' and meta_key = '".$header_image."'"; 
		$post_id = $wpdb->get_var($query);
		// Retrieves attachment id
		$query2= "SELECT meta_value FROM wp_postmeta WHERE post_id = '".$post_id."' AND meta_key='_wp_attached_file'";
		$attachment_id = $wpdb->get_var($attachment_id);
		// Retrieves image url fro attachment id
		$media_url = wp_get_attachment_url($attachment_id );
    }
    return  $media_url;
}	
// change product suffix 

//Add in text after price to certain products
add_filter( 'woocommerce_get_price_html', 'themeprefix_custom_price_message' );

function themeprefix_custom_price_message( $price ) { 
	
	global $post;
		
	$product_id = $post->ID;
	$my_product_array = array(746 );//add in product IDs
	if ( !in_array( $product_id, $my_product_array )) {
		$textafter = 'Starting at'; //add your text
		return '<span class="price-description">' . $textafter . '</span> '. $price;
	}
	 
	else { 
		return $price; 
	} 
}

add_filter( 'woocommerce_currency_symbol', 'change_currency_symbol', 10, 2 );

function change_currency_symbol( $symbols, $currency ) {
	if ( 'USD' === $currency ) {
		return 'US$';
	}
        return $symbols;
}


/**
 * Add the field to the checkout
 */
add_action( 'woocommerce_after_checkout_billing_form', 'custom_checkout_newsletter_subscription' );

function custom_checkout_newsletter_subscription( $checkout ) {

    echo '<div id="custom-checkout-newsletter-subscription-checkbox">';

    woocommerce_form_field( 'checkout_newsletter_checkbox', array(
        'type'          => 'checkbox',
        'class'         => array('checkout-newsletter form-row-wide'),
        'label'         => __('I would like to subscribe to IoTargeting news and updates!'),
        'required' 		=> false,
        'default' 		=> 1,
        ), $checkout->get_value( 'checkout_newsletter_checkbox' ));

    echo '</div>';
}

/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'custom_checkout_newsletter_subscription_update_order_meta' );

function custom_checkout_newsletter_subscription_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['checkout_newsletter_checkbox'] ) ) {
        update_post_meta( $order_id, 'Subscribed to newsletter', sanitize_text_field( $_POST['checkout_newsletter_checkbox'] ) );
    }
}

/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'custom_checkout_newsletter_subscription_display_admin_order_meta', 10, 1 );

function custom_checkout_newsletter_subscription_display_admin_order_meta($order){
   	$my_meta = get_post_meta( $order->ID, 'Subscribed to newsletter', true );
   	if($my_meta) {
   		echo '<p><strong>'.__('Subscribed to newsletter?').'</strong> ' . Yes . '</p>';
   	} 
}


/**
* Removes or edits the 'Protected:' part from posts titles
*/
 
add_filter( 'protected_title_format', 'remove_protected_text' );
function remove_protected_text() {
return __('%s');
}


/**
 * Change password strength
 */
function password_strength_meter_settings( $params, $handle  ) {
	if( $handle === 'wc-password-strength-meter' ) {
		$params = array_merge( $params, array(
			'min_password_strength' => 2,
			'i18n_password_error' => 'Please make your password stronger',
			'i18n_password_hint' => 'Your password must be <strong>at least 7 characters</strong> and contain a mix of <strong>UPPER</strong> and <strong>lowercase</strong> letters, <strong>numbers</strong>, and <strong>symbols</strong> (e.g., <strong> ! " ? $ % ^ & </strong>).'
		) );
	}
	return $params;
}

add_filter( 'woocommerce_get_script_data', 'password_strength_meter_settings', 20, 2);

/**
* Disable add to cart button  / Disable purchase of ALL products
**/



/**
*  INDIEGOGO ADD TO CART BUTTON
**/
add_filter( 'woocommerce_is_purchasable', '__return_false');

function remove_loop_button(){
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
}
add_action('init','remove_loop_button');

add_action('woocommerce_after_shop_loop_item','replace_add_to_cart');
function replace_add_to_cart() {
	global $product;
	$link = $product->get_permalink();
	$indiegogo_link = 'https://www.indiegogo.com/projects/iotargeting-wireless-smart-targeting-system';
	echo do_shortcode('<br><a class="btn" target="_blank" id="indiegogo_cart" href="' . $indiegogo_link . '">Support Us On Indiegogo</a>');
}
?>


