<?php /* Template Name: Dealer Product Info Template*/ ?>
<?php get_header(); ?>
<section id="dealer-product-info">
<div class="container">
    
    <?php 
    if ( have_posts() ) { 
        while ( have_posts() ) : the_post();
    ?>
    <div class="blog-post">
         <h2 class="blog-post-title"><?php the_title(); ?></h2>
        <p class="blog-post-meta"></p>
           <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-8 col-md-10 col-sm-offset-2 col-md-offset-1 centered">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
    </div><!-- /.blog-post -->
    <?php
        endwhile;
    } 
    ?>

</div>
</section>
<?php get_footer(); ?>