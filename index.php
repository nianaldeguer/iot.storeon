<?php get_header(); ?>
<div class="container">
    
    <?php 
    if ( have_posts() ) { 
        while ( have_posts() ) : the_post();
    ?>

     <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
         <?php 
            if (is_front_page() || is_search() ) {
        ?>
         <h2 class="entry-title-link"><a href="<?php the_permalink(); ?>" title="<?php printf( __('Permalink to %s', 'your-theme'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
        <?php
            }
            else {
        ?>
           <h2 class="entry-title"><?php the_title(); ?></h2>
         <?php 
            } 
         ?>
        <p class="blog-post-meta"></p>
        <div class="container">
            <?php the_content(); ?>
        </div>
    </div><!-- /.blog-post -->
    <?php
        endwhile;
    } 
    ?>

</div>

<?php get_footer(); ?>