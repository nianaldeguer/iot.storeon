//Retrieves orders
SELECT wp1.`post_id` AS 'Order Id', wp1.`meta_value` AS 'Email Address', 
		wp2.`meta_value` AS 'First Name', 
		wp3.`meta_value` AS 'Last Name', 
		wp4.`meta_value` AS 'Company Name',
		wp5.`meta_value` AS 'Order Complete Date'
FROM `wp_postmeta` wp1 LEFT JOIN 
	`wp_postmeta` wp2 ON wp1.`post_id`=wp2.`post_id` LEFT JOIN
	`wp_postmeta` wp3 ON wp1.`post_id`=wp3.`post_id` LEFT JOIN
	`wp_postmeta` wp4 ON wp1.`post_id`=wp4.`post_id` LEFT JOIN
	`wp_postmeta` wp5 ON wp1.`post_id`=wp5.`post_id`	
WHERE wp1.`meta_key`='_billing_email' 
AND wp2.`meta_key`='_billing_first_name' 
AND wp3.`meta_key`='_billing_last_name' 
AND wp4.`meta_key`='_billing_company'
AND wp5.`meta_key`='_date_completed'
ORDER BY 'Order Id';

//Retrieves orders with customers subscribed to newsletter
SELECT wp1.`post_id` AS 'Order Id', wp1.`meta_value` AS 'Email Address', 
		wp2.`meta_value` AS 'First Name', 
		wp3.`meta_value` AS 'Last Name', 
		wp4.`meta_value` AS 'Company Name',
		wp5.`meta_value` AS 'Order Complete Date'
FROM `wp_postmeta` wp1 LEFT JOIN 
	`wp_postmeta` wp2 ON wp1.`post_id`=wp2.`post_id` LEFT JOIN
	`wp_postmeta` wp3 ON wp1.`post_id`=wp3.`post_id` LEFT JOIN
	`wp_postmeta` wp4 ON wp1.`post_id`=wp4.`post_id` LEFT JOIN
	`wp_postmeta` wp5 ON wp1.`post_id`=wp5.`post_id`	
WHERE wp1.`post_id` IN (SELECT `post_id` FROM `wp_postmeta` WHERE `meta_key`='Subscribed to newsletter')
AND wp1.`meta_key`='_billing_email' 
AND wp2.`meta_key`='_billing_first_name' 
AND wp3.`meta_key`='_billing_last_name' 
AND wp4.`meta_key`='_billing_company'
AND wp5.`meta_key`='_date_completed'
ORDER BY 'Order Id';

	
