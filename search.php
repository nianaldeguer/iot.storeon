<?php get_header(); ?>
<div class="container">
    
    <?php 
    if ( have_posts() ) { 
        while ( have_posts() ) : the_post();
    ?>
	<h2 class="entry-title-link"><a href="<?php the_permalink(); ?>" title="<?php printf( __('Permalink to %s', 'your-theme'), the_title_attribute('echo') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
   <?php
        endwhile;
    
    ?>
  <?php } else {  ?>

  <h3 class="entry-title">Sorry, but nothing matched your search terms. Please try again with some different keywords.</h3>
   <?php } ?>
   
   
   <div class="blog-post">
       
        <p class="blog-post-meta"></p>
        <div class="container">
            <?php the_content(); ?>
        </div>
    </div><!-- /.blog-post -->
    

</div>

<?php get_footer(); ?>