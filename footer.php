
  <footer class="footer">
      <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-6">
              <h1>IoTARGETING</h1>
              <p>
                IoTargeting seeks to enhance the world of sports and revolutionize the future of global competition through the use of targeting technology and training systems. All our technologies have been developed from the ground up, with several patents already established and still more pending. These ground breaking technologies extend far into many other applications beyond that of target based systems.
              </p>
              <div id="sm-buttons-footer">
                <ul>
                  <!-- <a href="<?php the_field('facebook_button'); ?>" > -->
                  <a href="https://www.facebook.com/iotargeting"  target="_blank" >
                      <span class="fa-stack">
                        <i class="fa fa-circle fa-stack-2x icon-background"></i>
                        <i class="fa fa-facebook fa-stack-1x"></i>
                      </span>
                  </a>
                  <!-- <a href="<?php the_field('twitter_button'); ?>"> -->
                  <a href="https://www.twitter.com/iotargeting" target="_blank" >
                      <span class="fa-stack">
                        <i class="fa fa-circle fa-stack-2x icon-background"></i>
                        <i class="fa fa-twitter fa-stack-1x"></i>
                      </span>
                  </a>
                 <!--  <a href="<?php the_field('youtube_button'); ?>" > -->
                  <a href="https://www.youtube.com/channel/UCiOPKa5txMU5qTUUUvWq7Dg" target="_blank" >
                      <span class="fa-stack">
                        <i class="fa fa-circle fa-stack-2x icon-background"></i>
                        <i class="fa fa-youtube-play fa-stack-1x"></i>
                      </span>
                  </a>
				  <!-- <a href="<?php the_field('Instagram_button'); ?>"> -->
                  <a href="https://www.instagram.com/iotargeting" target="_blank" >
                      <span class="fa-stack">
                        <i class="fa fa-circle fa-stack-2x icon-background"></i>
                        <i class="fa fa-instagram fa-stack-1x"></i>
                      </span>
                  </a>
                </ul>
              </div>
            </div>
            <div class="col-sm-6 col-md-3">
              <h1>SHOP FOR</h1>
              <ul>
                <li>
                  <a href="<?php echo home_url(); ?>/targets">
                    <i class="fa fa-circle"></i>
                    Targets
                  </a>
                </li>
                <li>
                   <a href="<?php echo home_url(); ?>/product-category/packages">
                    <i class="fa fa-circle"></i>
                    Packages
                  </a>
                </li>
                <li>
                  <a href="<?php echo home_url(); ?>/product-category/accessories">
                    <i class="fa fa-circle"></i>
                    Accessories
                  </a>
                </li>
              </ul>
            </div>
            <div class="col-sm-6 col-md-3">
               <h1>RESOURCES</h1>
              <ul>
                <li>
                 <a href="<?php echo home_url(); ?>">
                    <i class="fa fa-circle"></i>
                    Home
                  </a>
                </li>
                <li>
                  <a href="<?php echo home_url(); ?>/news">
                    <i class="fa fa-circle"></i>
                    News
                  </a>
                </li>

                <li>
                  <a href="<?php echo home_url(); ?>/about">
                    <i class="fa fa-circle"></i>
                    About
                  </a>
                </li>

                <li>
                  <a href="<?php echo home_url(); ?>/contact-us">
                    <i class="fa fa-circle"></i>
                    Contact
                  </a>
                </li>
                 <li>
                  <a href="<?php echo home_url(); ?>/faq">
                    <i class="fa fa-circle"></i>
                    FAQ
                  </a>
                </li>
                <li>
                  <a href="<?php echo home_url(); ?>/dealer-contact-form"/>
                    <i class="fa fa-circle"></i>
                    Become a Dealer!
                  </a>
                </li>
              </ul>
            </div>
          </div>
      </div>

      <div class="container-fluid">
        <div id="copyright" class="row text-center">
          <div class="col-sm-12">
            <a href="http://iotargeting.com/">IoTargeting © 2018</a>
          </div>
        </div>
      </div>

  </footer>

<?php wp_footer(); ?>
  <script>

  //Equal height js
  $(function() {
    $('.box').matchHeight();
  });

  //Scroll
  var ua = window.navigator.userAgent;
  var iOS = !!ua.match(/iPad/i) || !!ua.match(/iPhone/i);
  var webkit = !!ua.match(/WebKit/i);
  var iOSSafari = iOS && webkit && !ua.match(/CriOS/i);

  //Checks browser and device
/*  if(iOSSafari) {
    scrollForiOS();
  } else {
    scrollDefault();
  }*/

  //Prevents double-tap on iOS devices
  function scrollForiOS() {
    //$('a[href*=#]:not([data-toggle="tab"])').each(function() {
    $('a[href*=#]:not([data-toggle="tab"])').not('[href="#"]').not('[href="#0"]').on("click touchend", function(e) {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
            || location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
               if (target.length) {
                 $('html,body').animate({
                     scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
  } 

  function scrollDefault() {
      $('a[href*="#"]').not('[href="#"]').not('[href="#0"]').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
            || location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
               if (target.length) {
                 $('html,body').animate({
                     scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
  }

  </script>
  </body>
</html>