  <?php /* Template Name: Targets Page Template*/ ?>
  <?php get_header(); ?>
  <section id="target-info">

      <?php 
    if ( have_posts() ) { 
        while ( have_posts() ) : the_post();
    ?>
    <div class="blog-post">
         <h2 class="blog-post-title"><?php the_title(); ?></h2>
        <p class="blog-post-meta"></p>
        <section id="featured-product-1">
         <div class="container">
              <div class="row">
                <div id="featured-product-1-image" class="col-sm-5">
                  <img src="<?php the_field('featured_product_1_image'); ?>" alt="Master target image"/>
                </div>

                <div id="featured-product-1-text" class="col-sm-7">
                  <h1><a href="<?php echo home_url(); ?>/product-category/packages/"><?php the_field('featured_product_1_name'); ?></a></h1>
                  <p><?php the_field('featured_product_1_description'); ?></p> 
                  <ul>
                      <li><i class="fa fa-circle"></i><?php the_field('feature1_product1'); ?></li>
                      <li><i class="fa fa-circle"></i><?php the_field('feature2_product1'); ?></li>
                      <li><i class="fa fa-circle"></i><?php the_field('feature3_product1'); ?></li>
                      <li><i class="fa fa-circle"></i><?php the_field('feature4_product1'); ?></li>
                  </ul>
                  <p><a class="btn" href="<?php echo home_url(); ?>/product-category/packages/">View Products<i class="fa fa-circle"></i></a></p>
                </div>
              </div>
            </div>
          </section>

          <section id="featured-product-2">
              <div class="container">
                <div class="row">
                  <div id="featured-product-2-image" class="col-sm-5 push-switch">
                    <img src="<?php the_field('featured_product_2_image'); ?>" alt="Smart target image" />
                  </div>
                  <div id="featured-product-2-text" class="col-sm-7 pull-switch">
                    <h1><a href="<?php echo home_url(); ?>/product-category/packages/"><?php the_field('featured_product_2_name'); ?></a></h1>
                    <p><?php the_field('featured_product_2_description'); ?></p> 
                    <ul>
                        <li><i class="fa fa-circle"></i><?php the_field('feature1_product2'); ?></li>
                        <li><i class="fa fa-circle"></i><?php the_field('feature2_product2'); ?></li>
                        <li><i class="fa fa-circle"></i><?php the_field('feature3_product2'); ?></li>
                        <li><i class="fa fa-circle"></i><?php the_field('feature4_product2'); ?></li>
                    </ul>
                    <p><a class="btn" href="<?php echo home_url(); ?>/product-category/packages/">View Products<i class="fa fa-circle"></i></a></p>
                  </div>
              </div>
            </div>
          </section>

          <section id="featured-product-3">
             <div class="container">
              <div class="row">
                <div id="featured-product-3-image" class="col-sm-5">
                  <img src="<?php the_field('featured_product_3_image'); ?>" alt="Just target image"/>
                </div>

                <div id="featured-product-3-text" class="col-sm-7">
                  <h1><a href="<?php echo home_url(); ?>/product-category/targets/"><?php the_field('featured_product_3_name'); ?></a></h1>
                  <p><?php the_field('featured_product_3_description'); ?></p> 
                  <ul>
                      <li><i class="fa fa-circle"></i><?php the_field('feature1_product3'); ?></li>
                      <li><i class="fa fa-circle"></i><?php the_field('feature2_product3'); ?></li>
                      <li><i class="fa fa-circle"></i><?php the_field('feature3_product3'); ?></li>
                      <li><i class="fa fa-circle"></i><?php the_field('feature4_product3'); ?></li>
                  </ul>
                  <p><a class="btn" href="<?php echo home_url(); ?>/product-category/targets/">View Product<i class="fa fa-circle"></i></a></p>
                </div>
              </div>
            </div>

          </section>
          <section id="game-modes">
             <div class="container">
              <h1>Game Modes</h1>
              <div class="row">
                <div class="column" id="column-1">
                  <img alt="game-modes_01" src="https://iotargeting.com/wp-content/uploads/2018/12/game-modes_01.png" />
                  <img alt="game-modes_03" src="https://iotargeting.com/wp-content/uploads/2018/12/game-modes_03.png" />
                  <img alt="game-modes_05" src="https://iotargeting.com/wp-content/uploads/2018/12/game-modes_05.png" />
                  <img alt="game-modes_07" src="https://iotargeting.com/wp-content/uploads/2018/12/game-modes_07.png" />
                  <img alt="game-modes_09" src="https://iotargeting.com/wp-content/uploads/2018/12/game-modes_09.png" />
                </div>
                <div class="column" id="column-2">
                  <img alt="game-modes_02" src="https://iotargeting.com/wp-content/uploads/2018/12/game-modes_02.png" />
                  <img alt="game-modes_04" src="https://iotargeting.com/wp-content/uploads/2018/12/game-modes_04.png" />
                  <img alt="game-modes_06" src="https://iotargeting.com/wp-content/uploads/2018/12/game-modes_06.png" />
                  <img alt="game-modes_08" src="https://iotargeting.com/wp-content/uploads/2018/12/game-modes_08.png" />
                  <img alt="game-modes_10" src="https://iotargeting.com/wp-content/uploads/2018/12/game-modes_10.png" />
                </div>
              </div>
            </div>
            <br>
            <br>
            <br>
          </section>
    </div><!-- /.blog-post -->
    


    <?php
        endwhile;
    } 
    ?>
   

  <?php get_footer(); ?>