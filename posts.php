<?php /* Template Name: Posts Template */ ?>
<?php get_header(); ?>
<section id="post">
<div class="container">
    
    <?php 
    if ( have_posts() ) { 
        while ( have_posts() ) : the_post();
    ?>
	
    <div class="blog-post">
        <h2 class="blog-post-title"><?php the_title(); ?></h2>
        <p class="blog-post-meta"></p>
           <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3 centered">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
    </div>
    <?php
        endwhile;
    } 
    ?>

</div>
</section>
<?php get_footer(); ?>