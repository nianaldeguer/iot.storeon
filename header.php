<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120783789-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-120783789-1');
  </script>
  
  <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php wp_head(); ?>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- Custom styles & js -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.min.css"/>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"></script>


</head>

<!-- Begin Constant Contact Active Forms -->
<script> var _ctct_m = "7e60aa05060db4ac5921b89c5f38f6e4"; </script>
<script id="signupScript" src="//static.ctctcdn.com/js/signup-form-widget/current/signup-form-widget.min.js" async defer></script>
<!-- End Constant Contact Active Forms -->

<body <?php body_class(); ?>>
<header>
  <section id="mainNav">
    <nav class="navbar navbar-default">
      <div class="container">
        <ul id="left-nav-ul" class="nav navbar-nav">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo home_url(); ?>/my-account">My Account</a>
          </li>
          <li>
            <a class="nav-link js-scroll-trigger" href="<?php echo home_url(); ?>/about">About</a>
          </li>
          <li>
            <a class="nav-link js-scroll-trigger" href="<?php echo home_url(); ?>/news">News</a>
          </li>      
          <li>
           <!--  <a class="nav-link js-scroll-trigger" href="#mailmunch-pop-608989">Subscribe</a> -->
           <a href="#" data-toggle="modal" data-target="#newsletterModal">Subscribe</a>

            <!-- Modal -->
             <div class="modal fade" id="newsletterModal" tabindex="-1" role="dialog" aria-labelledby="newsletterModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    <div class="ctct-inline-form" data-form-id="5daccc20-16d5-4f1e-8e4b-b141bf5a2c48"></div>
                  </div>
                </div>
              </div>
            </div> 
            <!-- Modal end-->

          </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <div class="navbar-left">
            <ul class="nav navbar-nav">
              <li>
                <?php global $woocommerce; ?>
                 <a class="nav-cart" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
                  <?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?>
                </a>
              </li>
              <li>
                <div class="navbar-form">
                <?php get_search_form(); ?>
                </div>
              </li>
              <li>
                  <div id="sm-buttons-header">
                  <!-- <a href="<?php the_field('facebook_button'); ?>" > -->
                  <a href="https://www.facebook.com/iotargeting" target="_blank">
                      <span class="fa-stack">
                        <i class="fa fa-circle fa-stack-2x icon-background"></i>
                        <i class="fa fa-facebook fa-stack-1x"></i>
                      </span>
                  </a>
                  <!-- <a href="<?php the_field('twitter_button'); ?>" > -->
                  <a href="https://www.twitter.com/iotargeting" target="_blank" >
                      <span class="fa-stack">
                        <i class="fa fa-circle fa-stack-2x icon-background"></i>
                        <i class="fa fa-twitter fa-stack-1x"></i>
                      </span>
                  </a>
                  <!-- <a href="<?php the_field('youtube_button'); ?>" > -->
                  <a href="https://www.youtube.com/channel/UCiOPKa5txMU5qTUUUvWq7Dg" target="_blank" >
                      <span class="fa-stack">
                        <i class="fa fa-circle fa-stack-2x icon-background"></i>
                        <i class="fa fa-youtube-play fa-stack-1x"></i>
                      </span>
                  </a>
				  <!-- <a href="<?php the_field('instagram_button'); ?>" > -->
                  <a href="https://www.instagram.com/iotargeting" target="_blank" >
                      <span class="fa-stack">
                        <i class="fa fa-circle fa-stack-2x icon-background"></i>
                        <i class="fa fa-instagram fa-stack-1x"></i>
                      </span>
                  </a>
              </div>
              </li>
            </ul>
          </div>
        </ul> 
      </div>
    </nav>
  </section>
  
  

  <section id="secondaryNav">
     <nav class="navbar navbar-inverse">
      <div class="container">
          <div class="navbar-header">
            <a href="<?php echo site_url(); ?>">
              <img src="<?php echo site_url(); ?>/wp-content/themes/iot/images/iot-logo.svg" alt="IoT Logo" class="logo"/>
            </a> 
          </div>
            <ul class="nav navbar-nav navbar-right">
              <li>
                  <a href="<?php echo home_url(); ?>/shop">
                  <i class="fa fa-circle"></i>            
                  Shop
                </a>
              </li>
              <li>
                <!-- <a href="<?php echo home_url(); ?>/product-category/targets"> -->
                  <a href="<?php echo home_url(); ?>/targets">
                  <i class="fa fa-circle"></i>
                  Targets
                </a>
              </li>
<!--               <li>
                <a href="<?php echo home_url(); ?>/product-category/accessories">
                  <i class="fa fa-circle"></i>
                  Accessories
                </a>
             </li> -->
<!--              <li>
                <a href="<?php echo home_url(); ?>/resources">
                  <i class="fa fa-circle"></i>
                  Resources
                  </a>
            </li> -->
            <li>
              <a href="<?php echo home_url(); ?>/dealer-application">
                <i class="fa fa-circle"></i>            
                  Become a Dealer
                </a>
              </li>
              <li>
            </ul>
      </div>
    </nav>
  </section>
  
  <?php if(is_front_page()) : ?>
    <a href ="https://www.indiegogo.com/projects/iotargeting-wireless-smart-targeting-system" target="_blank">
    <section id="header-img" style="background-image:url(<?php the_field('front_page_header_image'); ?>);">
  <?php else : ?>
    <section id="header-img" style="background-image:url(<?php echo get_header_image_custom() ?>);">
  <?php endif; ?>
    <div class="overlay"></div>
    <div class="container">
      <div id="header-text">
        <!-- <em>Practice. Enhance. Improve. </em>Targeting -->
        <h1><?php the_field('header_text'); ?> 
          <img src='<?php the_field('phone_image'); ?>' /> 
        </h1> 
      </div>
    </div>
  </section>
</a>
  
  
</header>
