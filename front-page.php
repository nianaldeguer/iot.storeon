<?php get_header(); ?>
 
  
<div class="main-content">
  <section id="cta">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 text-center">
          <!-- Uncomment this for indiegogo prelaunch/campaign is live -->
          <p><a href="<?php the_field('cta_link'); ?>" target="_blank"><?php the_field('cta_banner_text'); ?></a></p>
          <!-- Delete this line once indiegogo prelaunch/campaign is live -->
          <!-- <p><a href="#" data-toggle="modal" data-target="#newsletterModal"><?php the_field('cta_banner_text'); ?></a></p> -->
        </div>
      </div>
    </div>
  </section>

<!--    <section id="cta2">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 text-center">
            <p><a href="https://iotargeting.com/dealer-application/">Click here to become a certified dealer and/or event center!</a></p>
        </div>
      </div>
    </div>
  </section>  -->
   
  
  <section id="main-featured-product">
    <div class="container">
      <div class="row">
        <div id="main-featured-product-image" class="col-sm-6">
          <!-- <img src="<?php the_field('main_featured_product_image'); ?>" alt="Main featured image" /> -->
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/DVv8Bge0WNY?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen>
            </iframe>
          </div>
        </div>

        <div id="main-featured-product-text" class="col-sm-6">

          <h1><?php the_field('main_featured_product_name'); ?></h1>
          <p><?php the_field('main_featured_product_description'); ?></p> 
           <ul>
              <li><i class="fa fa-circle"></i><?php the_field('mainfeature1'); ?></li>
              <li><i class="fa fa-circle"></i><?php the_field('mainfeature2'); ?></li>
              <li><i class="fa fa-circle"></i><?php the_field('mainfeature3'); ?></li>
              <li><i class="fa fa-circle"></i><?php the_field('mainfeature4'); ?></li>
           </ul>
          <p><a class="btn" href="#just-target">Watch the video now<i class="fa fa-circle"></i></a></p>
        </div>
      </div>
    </div>
  </section>

  <section id="photo-strip">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-4 col-md-4 col-lg-2 ">
            <img src="<?php the_field('photostrip_1'); ?>" class="img-fluid" alt="Photo 1" />
        </div>
        <div class="col-xs-4 col-md-4 col-lg-2">
            <img src="<?php the_field('photostrip_2'); ?>" class="img-fluid" alt="Photo 2" />
        </div>
        <div class="col-xs-4 col-md-4 col-lg-2">
            <img src="<?php the_field('photostrip_3'); ?>" class="img-fluid" alt="Photo 3" />
        </div>
        <div class="col-xs-4 col-md-4 col-lg-2">
            <img src="<?php the_field('photostrip_4'); ?>" class="img-fluid" alt="Photo 4" />
        </div>
        <div class="col-xs-4 col-md-4 col-lg-2">
            <img src="<?php the_field('photostrip_5'); ?>" class="img-fluid" alt="Photo 5" />
        </div>
        <div class="col-xs-4 col-md-4 col-lg-2">
            <img src="<?php the_field('photostrip_6'); ?>" class="img-fluid" alt="Photo 6" />
        </div>
      </div>
    </div>
  </section>

  <section id="product-preview">
    <div class="product-preview container-fluid">
      <div class="row row-eq-height">

        <div id="product-preview-1" class="col-md-4">
          <div class="box">
            <h2><?php the_field('product_preview_1_name'); ?></h2>
            <p class="preview-desc"><?php the_field('product_preview_1_description'); ?></p>
            <p class="preview-link"><a href="<?php the_field('product_preview_1_link'); ?>">View Product<i class="fa fa-circle"></i></a></p>
          </div>
        </div>

        <div id="product-preview-2" class="col-md-4">
           <div class="box">
              <h2><?php the_field('product_preview_2_name'); ?></h2>
              <p class="preview-desc"><?php the_field('product_preview_2_description'); ?></p>
              <p class="preview-link"><a href="<?php the_field('product_preview_2_link'); ?>">View Product<i class="fa fa-circle"></i></a></p>
          </div>
        </div>

        <div id="product-preview-3" class="col-md-4">
          <div class="box">
            <h2><?php the_field('product_preview_3_name'); ?></h2>
            <p class="preview-desc"><?php the_field('product_preview_3_description'); ?></p>
            <p class="preview-link"><a href="<?php the_field('product_preview_3_link'); ?>">View Product<i class="fa fa-circle"></i></a></p>
          </div>
        </div>

      </div>
    </div>
  </section>

   <section id="how-it-works">
    <div class="container">
      <div class="row">
        <div id="how-it-works-text" class="col-sm-12 col-lg-7 push-switch">
          <h1><?php the_field('how_it_works_title'); ?></h1>
          <p><?php the_field('how_it_works_description'); ?></p> 
        </div>

        <div id="how-it-works-image" class="col-sm-12 col-lg-5 pull-switch">
          <img src="<?php the_field('how_it_works_image'); ?>" alt="How it works image"/> 
        </div>
      </div>
    </div>
  </section>
  
  <section id="headline-1" class="bg-primary" style="background-image:url(<?php the_field('headline_1_image'); ?>);">
    <div class="container">
    
            <div id="headline-caption">
              <h1 class="section-heading text-white centered"><?php the_field('headline_1_title'); ?></h1>
              <p class="text-faded mb-4 centered"><?php the_field('headline_1_description'); ?></p>
            </div>

      </div>
  </section>

  <section id="featured-products">
    <div class="container">
      <div class="row">
        <h1>Popular Products</h1>
        <div id="items" class="col-md-12 text-center">
          <?php echo do_shortcode('[featured_products limit="3" columns="3"]'); ?>
        </div>
        <p><a class="btn" href="<?php echo home_url(); ?>/shop">View All Products<i class="fa fa-circle"></i></a></p>
      </div>
    </div>
  </section>

  <!--<section id="headline-2" class="bg-primary">
    <div class="container">
      <div class="row">
        <div class="col-md-12 mx-auto text-center">
          <h1 class="section-heading text-white"><?php the_field('headline_2_title'); ?></h1>
        </div>
      </div>
        <div class="row">
            <div class="col-md-4 mx-auto text-center">
                <h2>Be Active</h2>
                <p class="text-faded mb-4"><?php the_field('headline_2_description_1'); ?></p>
            </div>

            <div class="col-md-4 mx-auto text-center">
                <h2>Improve Skills</h2>
                <p class="text-faded mb-4"><?php the_field('headline_2_description_2'); ?></p>
            </div>

            <div class="col-md-4 mx-auto text-center">
                <h2>Have Fun</h2>
                <p class="text-faded mb-4"><?php the_field('headline_2_description_3'); ?></p>
            </div>
        </div>
    </div>
  </section>-->

  
</div>
<?php get_footer(); ?>
